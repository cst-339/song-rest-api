package com.gcu;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/service")
public class SongRestService {

	@Autowired
	SongDataService service;
	
	@GetMapping(path="/songs")
	public ResponseEntity<?> getUsers() {
		try {
			List<SongModel> users = service.findAll();
			if (users==null)
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			else 
				return new ResponseEntity<>(users, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping(path="/withtitle")
	public ResponseEntity<?> getProductByName(@RequestParam(value="title", required=true) String redoneTitle) {
		try {
			SongModel song = service.findByTitle(redoneTitle);
			if (song==null)
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			else 
				return new ResponseEntity<>(song, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
}
