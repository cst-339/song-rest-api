package com.gcu;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SongDataService implements DataAccessInterface<SongModel> {

	@Autowired
	private SongRepository songRepository;


	public SongDataService(SongRepository songRepository) {
		this.songRepository = songRepository;

		
	}
	
	@Override
	public List<SongModel> findAll() {
		// TODO Auto-generated method stub
		try {
		List<SongModel> allSongs = new ArrayList<SongModel>();
		songRepository.findAll().forEach(allSongs::add);
		return allSongs;
		} catch (Exception e) {
			return new ArrayList<SongModel>();
		}
	}

	public SongModel findByTitle(String id) {
		try {
			return songRepository.findByRedoneTitle(id);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public boolean create(SongModel t) {
		try {
			songRepository.save(t);
		return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean update(SongModel t) {
		try {
			songRepository.save(t);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean delete(SongModel t) {
		try {
			songRepository.delete(t);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	

}
