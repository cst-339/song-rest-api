package com.gcu;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.CrudRepository;


public interface SongRepository extends MongoRepository<SongModel, String> {
	public SongModel findByRedoneTitle(String redoneTitle);
	
}
