package com.gcu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MilestoneUserRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(MilestoneUserRestApplication.class, args);
	}

}
